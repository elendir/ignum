# Ignum Auth Sandbox
Repo obsahuje 4 aplikace:
* [Identity provider](https://ignum-idp.eu/) - zajišťuje autentizaci a SSO dle standardu [OIDC](http://openid.net/connect/) (node.js aplikace).
* [Původní doména](https://ignum-puvodni.eu/) - simulace původní domény (webová aplikace v PHP/Nette).
* [Nová doména](https://ignum-nova.eu/) - simulace nové domény (single page app v javascriptu).
* [Mikroslužba DNS](http://ignum-dns.eu/api/me) - Ukázka mikroslužby, ze které Původní doména a Nová doména získávají uživatelská data, například o DNS (webová aplikace v PHP/Nette).

## SSO demo
* Jdi na [Původní doménu](https://ignum-puvodni.eu/) a přihlaš se (username `michal`, libovolné heslo).
* Jdi na [Novou doménu](https://ignum-nova.eu/) a přihlaš se.


## Instalace
### Identity provider
Použitá knihovna: [node-oidc-provider](https://github.com/panva/node-oidc-provider) (node.js).

* `cd idp`
* Přejmenuj `config_sample.neon` na `config.neon` a uprav konfiguraci
* `npm install`
* `node .`

### Původní doména
Použitá knihovna: [OpenID-Connect-PHP](https://github.com/jumbojett/OpenID-Connect-PHP).

* `cd puvodni-domena`
* Přejmenuj `config_sample.neon` na `config.neon` a uprav konfiguraci
* `composer install`


### Nová doména
Použitá knihovna: [oidc-client-js](https://github.com/IdentityModel/oidc-client-js).

* `cd nova-domena`
* Přejmenuj `config_sample.neon` na `config.neon` a uprav konfiguraci


### Mikroslužba DNS

* `cd microservice-dns`
* Přejmenuj `config_sample.neon` na `config.neon` a uprav konfiguraci
* `composer install`



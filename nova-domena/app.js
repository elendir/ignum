var config = neon.decode(readTextFile('//' + window.location.hostname + '/config.neon'))
var oidcSettings = {
  authority: config.get('idp'),
  client_id: config.get('clientId'),
  redirect_uri: config.get('redirectUrl'),
  response_type: 'id_token token',
  scope: 'openid profile account',
  // automaticSilentRenew:true,
  filterProtocolClaims: true,
  loadUserInfo: true
}


Oidc.Log.logger = console
Oidc.Log.level = Oidc.Log.DEBUG
var userManager = new Oidc.UserManager(oidcSettings)




// handle sign in
document.getElementById('sign-in-link').onclick = function(e) {
  e.preventDefault()

  userManager.signinRedirect()
  .catch(function(err) {
    console.log(err)
  })
}


// handle sign out
document.getElementById('sign-out-link').onclick = function(e) {
  e.preventDefault()
  Cookies.remove('user')

  userManager.signoutRedirect({post_logout_redirect_uri: config.get('redirectUrl')})
    .catch(function(err) {
      console.log(err)
    })
}




window.onload = function(e) {

  // login user if id token returned from idp
  if (getHashValue('id_token')) {
    userManager.signinRedirectCallback().then(function(user) {
      console.log('signed in', user)
      Cookies.set('user', user, {
        expires: 1/24 // 1 hour
      })
      showLoggedIn()

    }).catch(function(err) {
      console.log(err)
    })
  }

  // show user if logged in
  showLoggedIn()
}




/**
 * Read after-hash value from url
 */
function getHashValue(key) {
  var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
  return matches ? matches[1] : null;
}


/**
 * Show who is logged in
 */
function showLoggedIn() {
  var user = Cookies.getJSON('user')
  if (user) {
    document.getElementById('signed-out').style.display = 'none';
    document.getElementById('signed-in').style.display = 'block';

    var name = user.profile['http://ignum.cz/account'][0]['name'];
    document.getElementById('user').innerHTML = name;
  }
}


/**
 * Read local file
 * https://stackoverflow.com/a/14446538
 */
function readTextFile(file) {
  var req = new XMLHttpRequest()
  req.open("GET", file, false)
  req.send(null)
  return req.responseText
}
<?php

namespace App;

use Drahak\Restful\Application\Routes\ResourceRoute;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{

	/**
	 * @return RouteList
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		$router[] = new ResourceRoute('/api/me', 'Api:me');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}
}

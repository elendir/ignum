<?php

namespace App\Presenters;

use Drahak\Restful\Application\UI\ResourcePresenter;
use Drahak\Restful\IResource;
use GuzzleHttp\Client;
use Nette\DI\Container;
use Nette\Http\Request;


class ApiPresenter extends ResourcePresenter
{

	const USER_RESOURCES = [
		'58409' => 'míšovo data získaná z DNS služby'
	];

	const USER_NO_RESOURCE = 'žádný DNS záznam nenalezen';
	const USER_NOT_FROM_IPAS = 'toto není IPAS account';


	/**
	 * @inject
	 * @var Container
	 */
	public $container;

	/**
	 * @inject
	 * @var Request
	 */
	public $httpRequest;


	public function actionMe($provider)
	{
		$config = $this->container->getParameters();
		$auth = $this->httpRequest->getHeader('Authorization');

		$client = new Client(['verify' => FALSE]);
		$response = $client->request('GET', $config['idp'][$provider], [
			'headers' => ['Authorization' => $auth]
		]);




		$body = (string) $response->getBody();
		$body = json_decode($body);

//		dump($body);


		$ipasId = NULL;

		if ($provider === 'auth0') {
			$namespace = 'http://ignum.cz/account';

			if (isset($body->$namespace->ipasId)) {
				$ipasId = $body->$namespace->ipasId;
			}

		} else {
			if (isset($body->account->ipas->id)) {
				$ipasId = $body->account->ipas->id;
			}
		}


		if ($ipasId) {
			$dns = self::USER_NO_RESOURCE;
			if (isset(self::USER_RESOURCES[$ipasId])) {
				$dns = self::USER_RESOURCES[$ipasId];
			}

		} else {
			$dns = self::USER_NOT_FROM_IPAS;
		}


		$this->resource->dns = $dns;
		$this->sendResource(IResource::JSON);
	}
}

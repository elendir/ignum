<?php

namespace Ignum;

use GuzzleHttp\Client;


class DnsconApi
{

	const ZONES_URL = 'zones';


	/**
	 * @var string
	 */
	private $host;


	/**
	 * @param string $host
	 */
	public function __construct($host)
	{
		$this->host = $host;
	}


	/**
	 * @param string $accessToken
	 * @return string[][]
	 */
	public function getZones($accessToken)
	{
		$url = $this->host . self::ZONES_URL;

		$client = new Client(['verify' => FALSE]);

		$response = $client->request('GET', $url, [
			'headers' => ['Authorization' => 'Bearer ' . $accessToken]
		]);

		$body = (string) $response->getBody();
		$body = json_decode($body);

		$zones = [];
		foreach ($body->data->zones as $zone) {
			$zones[] = [
				'domain' => $zone->domain
			];
		}


		return $zones;
	}
}

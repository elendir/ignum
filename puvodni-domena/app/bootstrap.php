<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

$configurator->setDebugMode([
	'62.109.136.8', // ignum
	'62.109.143.61', // ignum
	'62.109.143.233', // ignum
	'89.176.78.220' // misa doma
]);
$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__ . '/../lib')
	->addDirectory(__DIR__)
	->addDirectory(__DIR__ . '/../../../../../php/ignum-idp')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;

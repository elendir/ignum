<?php

namespace App\Presenters;

use Auth0\SDK\API\Authentication;
use Auth0\SDK\Auth0;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\DI\Container;
use Tracy\Debugger;


class Auth0Presenter extends Presenter
{

	const IDP = 'auth0';

	const CLAIMS_NAMESPACE = 'http://ignum.cz/account';

	const SESSION_NAMESPACE = 'user';


	/**
	 * @inject
	 * @var Container
	 */
	public $container;


	public function actionDefault()
	{
		$config = $this->container->getParameters();
		$session = $this->getSession(self::SESSION_NAMESPACE);

		$this->template->user = NULL;
		if (isset($session->user)) {
			$this->template->user = $session->user;


			$client = new Client(['verify' => FALSE]);

			try {
				$response = $client->request('GET', $config['dnsMicroservice'] . '?provider=' . self::IDP, [
					'headers' => ['Authorization' => 'Bearer ' . $session->accessToken]
				]);

				$body = (string) $response->getBody();
				$body = json_decode($body);
				$this->template->dns = $body->dns;

			} catch (ServerException $e) {
				Debugger::barDump((string) $e->getResponse()->getBody(), 'microservice response');
				$this->template->dns = 'Server error';
			}
		}


		$this->template->auth0ClientId = $config['idp']['auth0']['clientId'];
		$this->template->auth0Domain = $config['idp']['auth0']['url'];
	}


	public function actionLogout()
	{
		$session = $this->getSession(self::SESSION_NAMESPACE);
//		$idToken = $session->idToken;
		$session->remove();

		$auth0 = $this->getAuth0Client();
		$auth0->logout();

		$config = $this->container->getParameters();
		$auth0Api = $this->getAuth0Api();
		$logoutUrl = $auth0Api->get_logout_link(
			$redirectUrl = $this->link('//default'),
			$config['idp'][self::IDP]['clientId'],
			TRUE
		);

		$this->redirectUrl($logoutUrl);
	}


	public function actionLogin($prompt = FALSE)
	{
		if (!$prompt) {
			$opts = [
				'prompt' => 'none'
			];

		} else {
			$opts = [
				'rememberLastLogin' => 0
			];
		}

		$auth0 = $this->getAuth0Client();
		$auth0->login(NULL, NULL, $opts);
//		$auth0->login();
	}


	/**
	 * @param string $error
	 */
	public function actionLoginCallback($error = NULL)
	{
		if ($error) {
			// retry auth with login prompt
			$auth0 = $this->getAuth0Client();
			$auth0->login();
		}


		$session = $this->getSession(self::SESSION_NAMESPACE);

		$auth0 = $this->getAuth0Client();
		$userInfo = $auth0->getUser();

		$account = $userInfo[self::CLAIMS_NAMESPACE];
		$session->user = $account['name'];
		$session->accessToken = $auth0->getAccessToken();
//		$session->idToken = $idToken;

		Debugger::barDump($userInfo, 'claims received');
		Debugger::barDump($session->accessToken, 'access token');
		$this->redirect('default');
	}


	/**
	 * @return Form
	 */
	protected function createComponentAuth0Form()
	{
		$form = new Form;

		$form->addText('username', 'Username')->setRequired();
		$form->addPassword('password', 'Heslo')->setRequired();

		$form->addSubmit('submit', 'Přihlásit');
		$form->onSuccess[] = [$this, 'loginFormSucceeded'];

		return $form;
	}


	/**
	 * @param Form $form
	 */
	public function loginFormSucceeded(Form $form)
	{
		$auth0Settings = $this->getAuth0ClientSettings();
		$auth0Api = $this->getAuth0Api();

		$values = $form->getValues();

		$options = [
			'username' => $values->username,
			'password' => $values->password,
//			'grant_type' => 'password'
//			'realm' => 'Username-Password-Authentication',
			'realm' => 'ipas',
		];

		$tokens = $auth0Api->login($options + $auth0Settings);
		$accessToken = $tokens['access_token'];


		$session = $this->getSession(self::SESSION_NAMESPACE);

		$userInfo = $auth0Api->userinfo($accessToken);

		$account = $userInfo[self::CLAIMS_NAMESPACE];
		$session->user = $account['name'];
		$session->accessToken = $accessToken;
		$session->provider = 'auth0';

		Debugger::barDump($userInfo, 'claims received');
		Debugger::barDump($session->accessToken, 'access token');
		$this->redirect('default');
	}


	/**
	 * @return Auth0
	 */
	private function getAuth0Client()
	{
		$config = $this->container->getParameters();

		$idpUrl = $config['idp'][self::IDP]['url'];
		$idpUrl = str_replace('https://', '', $idpUrl); // auth0 client requires url without protocol
		$idpUrl = trim($idpUrl, '/'); // auth0 client requires url without trailing slash

		$clientId = $config['idp'][self::IDP]['clientId'];
		$clientSecret = $config['idp'][self::IDP]['clientSecret'];

		$redirectUrl = $this->link('//loginCallback');

		$auth0 = new Auth0($this->getAuth0ClientSettings());

		return $auth0;
	}


	/**
	 * @return Authentication
	 */
	private function getAuth0Api()
	{
		$auth0Settings = $this->getAuth0ClientSettings();
		return new Authentication($auth0Settings['domain'], $auth0Settings['client_id'], $auth0Settings['client_secret']);
	}


		/**
	 * @return mixed[]
	 */
	private function getAuth0ClientSettings()
	{
		$config = $this->container->getParameters();

		$idpUrl = $config['idp'][self::IDP]['url'];
		$idpUrl = str_replace('https://', '', $idpUrl); // auth0 client requires url without protocol
		$idpUrl = trim($idpUrl, '/'); // auth0 client requires url without trailing slash

		$clientId = $config['idp'][self::IDP]['clientId'];
		$clientSecret = $config['idp'][self::IDP]['clientSecret'];

		$redirectUrl = $this->link('//loginCallback');

		return [
			'domain' => $idpUrl,
			'client_id' => $clientId,
			'client_secret' => $clientSecret,
			'redirect_uri' => $redirectUrl,
			'audience' => 'https://ignum.auth0.com/userinfo',
			'persist_id_token' => TRUE,
			'persist_access_token' => TRUE,
			'persist_refresh_token' => TRUE,
//			'scope' => 'openid profile http://ignum.namespace.com/account'
			'scope' => 'openid profile'
		];
	}
}



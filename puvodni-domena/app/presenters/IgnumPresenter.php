<?php

declare(strict_types = 1);

namespace App\Presenters;

use GuzzleHttp\Client;
use Ignum\IdentityProvider\IdentityProvider;
use Ignum\IdentityProvider\Identity\Identity;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\DI\Container;
use Tracy\Debugger;


class IgnumPresenter extends Presenter
{

	const IDP = 'ignum';

	const CLAIMS_NAMESPACE = 'identity';

	const SESSION_NAMESPACE = 'user';

	const DNSCON_API_URL = 'http://dnsca-auth.php56.dev.com.cz/';

	const
		METHOD_POST = 'POST',
		METHOD_XHR = 'XHR',
		METHOD_CURL = 'CURL',
		METHOD_IFRAME = 'IFRAME'
	;


	/**
	 * @inject
	 * @var Container
	 */
	public $container;

	/**
	 * @persistent
	 * @var string
	 */
	public $idp = self::IDP;


	public function actionDefault()
	{
		$config = $this->container->getParameters();
		$this->template->tokenEndpoint = $config['idp'][$this->idp]['url'] . '/token';
		$this->template->userinfoEndpoint = $config['idp'][$this->idp]['url'] . '/me';
		$this->template->clientId = $config['idp'][$this->idp]['clientId'];
		$this->template->clientSecret = $config['idp'][$this->idp]['clientSecret'];


		// retrieve user
		$session = $this->getSession(self::SESSION_NAMESPACE);


		$this->template->user = NULL;
		if (!isset($session->identity)) {
			return;
		}


		// refresh username
		$idp = $this->getIdentityProvider();
		$session->identity = $identity = $idp->getFreshIdentity($session->identity, true);
		Debugger::barDump($identity, 'claims received');
		$this->template->user = $identity->hasIpas() ? $identity->getIpas()->getName() : $identity->getMojeId()->getName();


//		$apiIdentity = $idp->useAccessToken($identity->getAuthentication()->getAccessToken());
//		Debugger::barDump($apiIdentity, 'apiIdentity');


//		$dnsconApi = new DnsconApi(self::DNSCON_API_URL);
//
//		$this->template->zones = [];
//		try {
//			$this->template->zones = $dnsconApi->getZones($session->accessToken);
//
//		} catch (BadResponseException $e) {
//			Debugger::barDump((string) $e->getResponse()->getBody(), 'dnsconapi response');
//		}
		$this->template->zones = [];
	}


	public function actionLogin()
	{
		$idp = $this->getIdentityProvider();
		$identity = $idp->authenticate();

		Debugger::barDump($identity, 'claims received');

		$session = $this->getSession(self::SESSION_NAMESPACE);
//		$session->setExpiration($payload->exp);
		$session->setExpiration(3600 * 2);
		$session->identity = $identity;

		$this->redirect('default');
	}


	public function actionFrontChannelLogout()
	{
		$session = $this->getSession(self::SESSION_NAMESPACE);
		$session->remove();
	}


	public function actionImpersonation(string $subjectToken)
	{
//		$this->useTokenExchange($subjectToken);
		$this->getIdentityProvider()->requestImpersonation($subjectToken);
	}


	public function actionLogout()
	{
		$redirectUrl = $this->link('//default', ['bla' => 123, 'idp' => NULL]);
		$idp = $this->getIdentityProvider();
		$session = $this->getSession(self::SESSION_NAMESPACE);

		if (isset($session->identity)) {
			$identity = clone $session->identity;
			$session->remove();
			$idp->requestLogout($identity, $redirectUrl);
		}

		$this->redirectUrl($redirectUrl);
	}


	public function actionSocial($provider)
	{
		$idp = $this->getIdentityProvider();
		$idp->requestAuthorization($provider);
	}


	public function actionRequestImpersonation(int $id)
	{
		$this->requestImpersonation($id);
	}


	private function getIdentityProvider(): IdentityProvider
	{
		$config = $this->container->getParameters();

		$idpUrl = $config['idp'][$this->idp]['url'];
		$clientId = $config['idp'][$this->idp]['clientId'];
		$clientSecret = $config['idp'][$this->idp]['clientSecret'];

		$redirectURL = $this->link('//login', ['idp' => NULL]);

		$idp = new IdentityProvider($idpUrl, $clientId, $clientSecret, $redirectURL, FALSE);
		return $idp;
	}


	/**
	 * @return Form
	 */
	protected function createComponentImpersonateForm()
	{
		$form = new Form;

		$form->addText('id', 'Account id')->setRequired();

		$form->addSubmit('submit', 'Impersonate');
		$form->onSuccess[] = [$this, 'impersonateFormSucceeded'];

		return $form;
	}


	/**
	 * @param Form $form
	 */
	public function impersonateFormSucceeded(Form $form)
	{
		$values = $form->getValues();
		$this->requestImpersonation((int) $values->id);
	}


	/**
	 * @return Form
	 */
	protected function createComponentLoginForm()
	{
		$form = new Form;

		$form->addText('username', 'Username');
		$form->addPassword('password', 'Password');
		$form
			->addRadioList('method', 'Method', [
				self::METHOD_POST => 'POST',
				self::METHOD_XHR => 'XHR',
				self::METHOD_CURL => 'CURL',
				self::METHOD_IFRAME => 'IFRAME'
			])
			->setDefaultValue(self::METHOD_POST)
			->setRequired();


		// parametry pro post form
		$idp = $this->getIdentityProvider();
		foreach($idp->getLoginFormParams() as $param => $value) {
			$form->addHidden($param)->setValue($value);
		}
		$form->addHidden('action')->setValue($idp->getAuthorizationEndpoint());


		$form->addSubmit('submit', 'Sign in');
		$form->onSuccess[] = [$this, 'loginFormSucceeded'];

		return $form;
	}


	public function loginFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		if ($values->method === self::METHOD_CURL) {
			$this->useResourceOwnerPassword($values->username, $values->password);

		} else {
			die('tudy ne');
		}
	}


	private function useResourceOwnerPassword(string $username, string $password)
	{
		$idp = $this->getIdentityProvider();

		$identity = $idp->useResourceOwnerPassword($username, $password);
		Debugger::barDump($identity, 'identity');


		// login
		$session = $this->getSession(self::SESSION_NAMESPACE);
		$session->setExpiration(3600);
		$session->identity = $identity;


		$this->redirect('default');
	}


	private function useTokenExchange(string $subjectToken)
	{
		$idp = $this->getIdentityProvider();

		$identity = $idp->useTokenExchange($subjectToken);
		Debugger::barDump($identity, 'identity');


		// login
		$session = $this->getSession(self::SESSION_NAMESPACE);
		$session->setExpiration(3600);
		$session->identity = $identity;


		$this->redirect('default');
	}


	private function requestImpersonation(int $id)
	{
		$session = $this->getSession(self::SESSION_NAMESPACE);
		$idp = $this->getIdentityProvider();
		$url = 'http://puvodni.domena.here/ignum/impersonation';


		$idp->redirectToClientImpersonationUrl($session->identity, $id, $url);


//		$impersonationToken = $idp->getImpersonationToken($session->identity, $id);
//		$url .= '?subjectToken=' . $impersonationToken;
//		$this->redirectUrl($url);



//		$subjectToken = $this->useImpersonatorToken($impersonator->getAccessToken(), $ipasId);
//		$url = $clientImpersonationUrl . '?subjectToken=' . $subjectToken;
//		self::redirect($url);


	}
}



<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

//		$router[] = new Route('/login/<provider>', 'Homepage:login');


		$router[] = new Route('//[www.]<domain puvodni3.domena.here>/<presenter>/<action>', [
			'presenter' => 'Homepage',
			'action'    => 'default',
			'idp' => 'ignum.3'
		]);



//		$router[] = new Route('<presenter>/<action>[/<idp>]', 'Homepage:default');
		$router[] = new Route('<presenter>/<action>', 'Homepage:default');


		return $router;
	}
}


// https://stackoverflow.com/questions/9899372/pure-javascript-equivalent-of-jquerys-ready-how-to-call-a-function-when-t/9899701#9899701
(function() {

  document.getElementById('frm-loginForm').addEventListener('submit', function(e) {
    var method = this.querySelectorAll('[name=method]:checked')[0].value

    if (method === 'XHR') {
      e.preventDefault()

      var username = this.querySelectorAll('[name=username]')[0].value
      var password = this.querySelectorAll('[name=password]')[0].value

      useCredentials(username, password)

    } else if (method === 'POST' || method === 'IFRAME') {
      e.preventDefault()
      var form = document.getElementById('frm-loginForm')
      var action = form.querySelector('input[name="_action"]')
      form.setAttribute('action', action.getAttribute('value'))

      if (method === 'IFRAME') {
        form.setAttribute('target', 'auth_iframe')
      }

      form.submit()
    }
  })

})()




// var config = {
//   idp: {
//     tokenEndpoint: 'https://id-test.domena.cz/token',
//     userinfoEndpoint: 'https:id-test.domena.cz/me'
//   },
//   client: {
//     id: 'novadomena',
//     secret: 'f3db28aa09aefed5adf8033948ad4a1796e445'
//   }
// }




function useCredentials(username, password) {
  var xhr = new XMLHttpRequest()
  xhr.open('POST', config.idp.tokenEndpoint)
  xhr.withCredentials = true

  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
  xhr.setRequestHeader('Authorization', 'Basic ' + btoa(config.client.id + ':' + config.client.secret))

  xhr.onload = function () {
    if (xhr.status === 200) {
      console.log('OK token', xhr.responseText)
      // getUserinfo(JSON.parse(xhr.responseText).access_token)

    } else {
      console.log('FAIL token', xhr.status)
    }
  }

  var qs =
    'grant_type=resource_owner_password'
    + '&response_type=id_token token'
    + '&scope=openid identity'
    + '&client_id=' + config.client.id
    + '&username=' + username
    + '&password=' + password
  ;

  xhr.send(encodeURI(qs))
}


function useImpersonator(accessToken, id) {
  var xhr = new XMLHttpRequest()
  xhr.open('POST', config.idp.tokenEndpoint)
  xhr.withCredentials = true

  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
  xhr.setRequestHeader('Authorization', 'Basic ' + btoa(config.client.id + ':' + config.client.secret))

  xhr.onload = function () {
    if (xhr.status === 200) {
      console.log('OK token', xhr.responseText)
      getUserinfo(JSON.parse(xhr.responseText).access_token)

    } else {
      console.log('FAIL token', xhr.status)
    }
  }

  var qs =
    'grant_type=impersonator'
    + '&response_type=id_token token'
    + '&scope=openid identity'
    + '&client_id=' + config.client.id
    + '&access_token=' + accessToken
    + '&impersonate_ipas_id=' + id
  ;

  xhr.send(encodeURI(qs))
}


function getUserinfo(accessToken) {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', config.idp.userinfoEndpoint)
  xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken)
  xhr.onload = function () {
    if (xhr.status === 200) {
      console.log('OK userinfo', xhr.responseText)

    } else {
      console.log('FAIL userinfo', xhr.status)
    }
  }

  xhr.send()
}
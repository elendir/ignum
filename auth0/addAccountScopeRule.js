function (user, context, callback) {
  // The currently requested scopes can be accessed as follows:
  // context.request.query.scope.match(/\S+/g)
  var scopeMapping = {
    account: ['account']
  };
  context.jwtConfiguration.scopes = scopeMapping;
  callback(null, user, context);
}
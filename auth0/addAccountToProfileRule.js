function (user, context, callback) {
  var account = {
    username: user.nickname,
    name: user.name,
    ipasId: user.user_id.replace('auth0|','')
  };

  // var namespace = 'http://ignum.namespace.com/';
  var namespace = 'http://ignum.cz';

  context.idToken = context.idToken || {};
  context.idToken[namespace + '/account'] = account;

  callback(null, user, context);
}